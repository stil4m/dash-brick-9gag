import cherrypy

from app.controllers import BaseController
from jinja2 import Environment, FileSystemLoader

class GagsController(BaseController):

    global NINEGAG_PLUGIN_NAME

    """9Gag controller"""
    def __init__(self, feed, prefix):
        BaseController.__init__(self)
        self.feed = feed
        self.env = Environment(loader=FileSystemLoader(prefix.lstrip('/') + '/templates'))

    @cherrypy.expose
    def index(self):
        template = self.env.get_template('index.html')
        return template.render(gags=self.feed['entries'])