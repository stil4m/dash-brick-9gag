function ninegag_class(name) {

    var thisObject = this;
    this.name = name;
    var elementID = this.name;
    var path = '/plugins/' + elementID;


    this.render = function() {
        $.get(
            path + '/',
            function(responseText){
                $("#"+elementID).html(responseText);
            },
            "html"
        );
    }

};