from controllers import GagsController
import feedparser

class Brick():

    def __init__(self, mapper, prefix, path):
        self.mapper = mapper
        self.prefix = prefix
        self.name = self.prefix.split('/')[-1]
        self.path = path
        self.update()
        self.setup_mapper()
        
    
    def setup_mapper(self):
        if self.prefix and self.mapper:
            gags_controller = GagsController(self.gags, self.prefix)
            mapper_name = 'dash-brick-' + self.name
            self.mapper.connect(name=mapper_name, route=(self.prefix + '/'), controller=gags_controller, action = 'index')

    def update(self):
        self.gags = feedparser.parse('http://tumblr.9gag.com/rss');

    def get_javascript(self):
        js = "<script type=\"text/javascript\" src=\""+ self.prefix + "/brick.js\"></script>\n"
        js += "<script> var " + self.name + " = new ninegag_class('"+ self.name +"')</script>\n"
        js += "<script>" + self.name + ".render()</script>"
        return js

    def timeout(self):
#        Suitable timeout until update
        return 60

    def get_real_name(self):
        return 'dash-brick-9gag'